<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Changelog;

use Psr\Log\LoggerInterface;
use ReflectionClass;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Serializer\SerializerInterface;
use WebSupport\Messenger\BoundedContext\BoundedContextNameParser;
use WebSupport\Messenger\Message\DomainMessageStamp;
use WebSupport\Messenger\UserInfo\UserInfoStamp;

final class ChangelogMiddleware implements MiddlewareInterface
{
    private LoggerInterface $changelogLogger;
    private SerializerInterface $serializer;

    /**
     * @param LoggerInterface $changelogLogger must be named according to monolog channel
     *     https://symfony.com/doc/current/logging/channels_handlers.html#how-to-autowire-logger-channels
     */
    public function __construct(LoggerInterface $changelogLogger, SerializerInterface $serializer)
    {
        $this->changelogLogger = $changelogLogger;
        $this->serializer = $serializer;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        /** @var ChangelogStamp|null $loggerStamp */
        $loggerStamp = $envelope->last(ChangelogStamp::class);

        if ($loggerStamp !== null) {
            return $stack->next()->handle($envelope, $stack);
        }

        /** @var DomainMessageStamp|null $domainMessageStamp */
        $domainMessageStamp = $envelope->last(DomainMessageStamp::class);

        if ($domainMessageStamp === null) {
            // nothing to do here, not a domain message
            return $stack->next()->handle($envelope, $stack);
        }

        $message = $envelope->getMessage();
        $context = [
            'id' => $domainMessageStamp->getAggregateId(),
            'context' => BoundedContextNameParser::parseFromClassName(get_class($message)),
            'messageType' => $domainMessageStamp->getMessageType(),
            'messageName' => (new ReflectionClass($message))->getShortName(),
        ];

        if (null !== ($userInfo = $envelope->last(UserInfoStamp::class))) {
            assert($userInfo instanceof UserInfoStamp);
            $context += [
                'userLogin' => $userInfo->getLogin(),
                'userIp' => $userInfo->getIp(),
                'userId' => $userInfo->getId(),
            ];
        }

        $this->changelogLogger->info($this->serializer->serialize($message, 'json'), $context);
        $envelope = $envelope->with(new ChangelogStamp());

        return $stack->next()->handle($envelope, $stack);
    }
}
