<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Changelog;

use Symfony\Component\Messenger\Stamp\StampInterface;

final class ChangelogStamp implements StampInterface
{
}
