<?php

declare(strict_types=1);

namespace WebSupport\Messenger\UserInfo;

use Symfony\Component\Messenger\Stamp\StampInterface;

final class UserInfoStamp implements StampInterface
{
    private string $id;
    private string $ip;
    private string $login;

    public function __construct(string $id, string $ip, string $login)
    {
        $this->id = $id;
        $this->ip = $ip;
        $this->login = $login;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function getLogin(): string
    {
        return $this->login;
    }
}
