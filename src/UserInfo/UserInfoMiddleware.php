<?php

declare(strict_types=1);

namespace WebSupport\Messenger\UserInfo;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

final class UserInfoMiddleware implements MiddlewareInterface
{
    private UserContextFactory $contextFactory;
    private ?UserContext $context = null;

    public function __construct(UserContextFactory $contextFactory)
    {
        $this->contextFactory = $contextFactory;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        // skip messages that already have user context
        if (null !== ($userInfo = $envelope->last(UserInfoStamp::class))) {
            if ($this->context === null) {
                assert($userInfo instanceof UserInfoStamp);
                $this->context = new UserContext($userInfo->getId(), $userInfo->getIp(), $userInfo->getLogin());
            }

            return $stack->next()->handle($envelope, $stack);
        }

        // try to to fetch context from the current process
        if ($this->context === null) {
            $this->context = $this->contextFactory->createUserContext();
        }

        $processedEnvelope = $stack->next()->handle(
            $envelope->with(
                new UserInfoStamp($this->context->getId(), $this->context->getIp(), $this->context->getLogin())
            ),
            $stack
        );

        // reset context for the next message
        $this->context = null;

        return $processedEnvelope;
    }
}
