<?php

declare(strict_types=1);

namespace WebSupport\Messenger\UserInfo;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

final class SymfonyUserContextFactory implements UserContextFactory
{
    private RequestStack $requestStack;
    private Security $security;

    public function __construct(RequestStack $requestStack, Security $security)
    {
        $this->requestStack = $requestStack;
        $this->security = $security;
    }

    public function createUserContext(): UserContext
    {
        // user id not sent with sdk requests (access tokens contain only login)
        return new UserContext('', $this->getIp(), $this->getLogin());
    }

    private function getLogin(): string
    {
        $user = $this->security->getUser();

        return $user ? $user->getUsername() : '';
    }

    private function getIp(): string
    {
        $request = $this->requestStack->getMasterRequest();
        if ($request === null) {
            return '';
        }

        $clientIps = $request->getClientIps();

        return end($clientIps);
    }
}
