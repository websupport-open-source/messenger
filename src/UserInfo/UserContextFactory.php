<?php

declare(strict_types=1);

namespace WebSupport\Messenger\UserInfo;

interface UserContextFactory
{
    public function createUserContext(): UserContext;
}
