<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Correlation;

use WebSupport\Messenger\Message\DomainMessageStamp;

use function array_pop;
use function count;

final class CorrelationIdProvider
{
    /**
     * Each message in the list is an array [$messageId(, $messageType, $aggregateId)]
     * Only the first value ($messageId) is guaranteed
     *
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingTraversablePropertyTypeHintSpecification
     */
    private array $causationVector = [];

    public function addMessageId(string $messageId): void
    {
        $this->causationVector[] = [$messageId];
    }

    public function addMessageFromStamp(DomainMessageStamp $stamp): void
    {
        $this->causationVector[] = [$stamp->getMessageId(), $stamp->getMessageType(), (string) $stamp->getAggregateId()];
    }

    public function getCurrentAggregateId(): ?string
    {
        [, , $aggregateId] = $this->getCurrentMessage();

        return $aggregateId;
    }

    public function getCurrentMessageId(): ?string
    {
        [$messageId] = $this->getCurrentMessage();

        return $messageId;
    }

    public function getCurrentMessageType(): ?string
    {
        [, $messageType] = $this->getCurrentMessage();

        return $messageType;
    }

    public function getCausationMessageId(): ?string
    {
        $messageCount = count($this->causationVector);

        if ($messageCount < 2) {
            return null;
        }

        [$causationId] = $this->causationVector[$messageCount - 2];

        return $causationId;
    }

    public function end(): void
    {
        array_pop($this->causationVector);
    }

    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingTraversableReturnTypeHintSpecification
     */
    private function getCurrentMessage(): ?array
    {
        $messageCount = count($this->causationVector);

        if ($messageCount === 0) {
            return null;
        }

        return $this->causationVector[$messageCount - 1];
    }
}
