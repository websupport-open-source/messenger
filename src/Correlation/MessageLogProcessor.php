<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Correlation;

use Monolog\Processor\ProcessorInterface;

use function is_object;

final class MessageLogProcessor implements ProcessorInterface
{
    public const MONOLOG_CHANNEL = 'messenger';

    private CorrelationIdProvider $idProvider;

    public function __construct(CorrelationIdProvider $idProvider)
    {
        $this->idProvider = $idProvider;
    }

    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingTraversableTypeHintSpecification
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.ReturnTypeHint.MissingTraversableTypeHintSpecification
     */
    public function __invoke(array $record): array
    {
        if ($record['channel'] !== self::MONOLOG_CHANNEL) {
            return $record;
        }

        $message = $record['context']['message'] ?? null;

        if ($message === null || !is_object($message)) {
            return $record;
        }

        $record['extra']['aggregate_id'] = $this->idProvider->getCurrentAggregateId();
        $record['extra']['msg_id'] = $this->idProvider->getCurrentMessageId();
        $record['extra']['msg_type'] = $this->idProvider->getCurrentMessageType();
        $record['extra']['msg_cause_id'] = $this->idProvider->getCausationMessageId();

        return $record;
    }
}
