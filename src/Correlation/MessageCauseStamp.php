<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Correlation;

use Symfony\Component\Messenger\Stamp\StampInterface;

final class MessageCauseStamp implements StampInterface
{
    private string $messageCauseId;

    public function __construct(string $messageCauseId)
    {
        $this->messageCauseId = $messageCauseId;
    }

    public function getMessageCauseId(): string
    {
        return $this->messageCauseId;
    }
}
