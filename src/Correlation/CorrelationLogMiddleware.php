<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Correlation;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;
use WebSupport\Messenger\Message\DomainMessageStamp;

/**
 * Looks for DomainMessageStamp and populates monolog CorrelationIdProvider
 * For sent messages it also adds MessageCauseStamp with the cause figured out in the current process
 * For received messages it reads the MessageCauseStamp and sets the cause message id to the provider
 */
class CorrelationLogMiddleware implements MiddlewareInterface
{
    private CorrelationIdProvider $idProvider;

    public function __construct(CorrelationIdProvider $idProvider)
    {
        $this->idProvider = $idProvider;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        /** @var DomainMessageStamp|null $domainMessageStamp */
        $domainMessageStamp = $envelope->last(DomainMessageStamp::class);

        if ($domainMessageStamp === null) {
            // nothing to do here, not a domain message
            return $stack->next()->handle($envelope, $stack);
        }

        /** @var MessageCauseStamp $messageCauseStamp */
        $messageCauseStamp = $envelope->last(MessageCauseStamp::class);

        $receivedMessageCauseId = null;

        if ($envelope->all(ReceivedStamp::class) && $messageCauseStamp !== null) {
            $receivedMessageCauseId = $messageCauseStamp->getMessageCauseId();
        }

        if ($receivedMessageCauseId !== null) {
            $this->idProvider->addMessageId($messageCauseStamp->getMessageCauseId());
        }

        // configure id provider with current message
        $this->idProvider->addMessageFromStamp($domainMessageStamp);

        // let the id provider figure out the causation and attach it
        $computedMessageCauseId = $this->idProvider->getCausationMessageId();

        if ($messageCauseStamp === null && $computedMessageCauseId !== null) {
            $envelopeWithCause = $envelope->with(new MessageCauseStamp($computedMessageCauseId));
        }

        $result = $stack->next()->handle($envelopeWithCause ?? $envelope, $stack);

        if ($receivedMessageCauseId !== null) {
            $this->idProvider->end();
        }

        $this->idProvider->end();

        return $result;
    }
}
