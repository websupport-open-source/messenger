<?php

namespace WebSupport\Messenger\Listener;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\Event\WorkerMessageFailedEvent;
use Symfony\Component\Messenger\Event\WorkerRunningEvent;
use Throwable;

final class StopWorkerOnClosedEntityManagerListener implements EventSubscriberInterface
{
    private LoggerInterface $logger;
    private bool $emClosed = false;
    private EntityManagerInterface $entityManager;
    private Throwable $throwable;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    public function onMessageFailed(WorkerMessageFailedEvent $event): void
    {
        $this->emClosed = !$this->entityManager->isOpen();
        $this->throwable = $event->getThrowable();
    }

    public function onWorkerRunning(WorkerRunningEvent $event): void
    {
        if ($this->emClosed && !$event->isWorkerIdle()) {
            $event->getWorker()->stop();
            $flattenedException = FlattenException::createFromThrowable($this->throwable);
            $this->logger->warning('Worker stopped Because Entity Manager was closed', ['exception' => $flattenedException->getAsString()]);
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            WorkerMessageFailedEvent::class => 'onMessageFailed',
            WorkerRunningEvent::class => 'onWorkerRunning',
        ];
    }
}
