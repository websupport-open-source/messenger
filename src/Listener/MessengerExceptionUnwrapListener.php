<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;

final class MessengerExceptionUnwrapListener implements EventSubscriberInterface
{
    /**
     * @return string[][]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['unwrapException', 100],
        ];
    }

    public function unwrapException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        do {
            if ($exception instanceof HandlerFailedException) {
                continue;
            }

            if ($exception instanceof UnrecoverableMessageHandlingException) {
                continue;
            }

            $event->setThrowable($exception);

            return;

            //phpcs:ignore SlevomatCodingStandard.ControlStructures.DisallowYodaComparison.DisallowedYodaComparison, SlevomatCodingStandard.ControlStructures.AssignmentInCondition.AssignmentInCondition
        } while (null !== $exception = $exception->getPrevious());
    }
}
