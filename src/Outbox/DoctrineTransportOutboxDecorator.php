<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Outbox;

use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Bridge\Doctrine\Transport\DoctrineTransport;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\Transport\Receiver\ListableReceiverInterface;
use Symfony\Component\Messenger\Transport\Receiver\MessageCountAwareInterface;
use Symfony\Component\Messenger\Transport\SetupableTransportInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use WebSupport\Messenger\BoundedContext\BoundedContextNameParser;
use WebSupport\Messenger\Message\PublicEvent;

/**
 * Forces dispatching for all public events
 * Throws away events that don't have private handlers and are not marked as public
 */
class DoctrineTransportOutboxDecorator implements TransportInterface, SetupableTransportInterface, MessageCountAwareInterface, ListableReceiverInterface
{
    private HandlersLocator $handlersLocator;
    private DoctrineTransport $doctrineTransport;
    private LoggerInterface $logger;

    public function __construct(DoctrineTransport $doctrineTransport, HandlersLocator $handlersLocator, LoggerInterface $logger)
    {
        $this->doctrineTransport = $doctrineTransport;
        $this->handlersLocator = $handlersLocator;
        $this->logger = $logger;
    }

    public function send(Envelope $envelope): Envelope
    {
        $message = $envelope->getMessage();

        if ($message instanceof PublicEvent) {
            return $this->doctrineTransport->send($envelope);
        }

        // for private events only send to outbox queue if there is local handler within the BC of the event itself
        $handlerDescriptors = $this->handlersLocator->getHandlers($envelope);
        $eventBoundedContext = BoundedContextNameParser::parseFromClassName(get_class($message));

        foreach ($handlerDescriptors as $handlerDescriptor) {
            $handlerBoundedContext = BoundedContextNameParser::parseFromMessengerHandlerDescriptor($handlerDescriptor);

            if ($eventBoundedContext === $handlerBoundedContext) {
                return $this->doctrineTransport->send($envelope);
            }
        }

        $this->logger->debug(
            'Discarding message "{className}" as no local handler exists within the Bounded Context.',
            ['className' => get_class($message), 'message' => $message]
        );

        return $envelope;
    }

    /**
     * @return Envelope[]
     */
    public function all(?int $limit = null): iterable
    {
        return $this->doctrineTransport->all($limit);
    }

    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
     */
    public function find($id): ?Envelope
    {
        return $this->doctrineTransport->find($id);
    }

    public function getMessageCount(): int
    {
        return $this->doctrineTransport->getMessageCount();
    }

    /**
     * @return Envelope[]
     */
    public function get(): iterable
    {
        return $this->doctrineTransport->get();
    }

    public function ack(Envelope $envelope): void
    {
        $this->doctrineTransport->ack($envelope);
    }

    public function reject(Envelope $envelope): void
    {
        $this->doctrineTransport->reject($envelope);
    }

    public function setup(): void
    {
        $this->doctrineTransport->setup();
    }
}
