<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Outbox;

use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Messenger\Bridge\Doctrine\Transport\DoctrineTransport;
use Symfony\Component\Messenger\Bridge\Doctrine\Transport\DoctrineTransportFactory;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;

class OutboxTransportFactory extends DoctrineTransportFactory
{
    use LoggerAwareTrait;

    private HandlersLocator $handlersLocator;

    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingParameterTypeHint
     */
    public function __construct($registry, HandlersLocator $handlersLocator)
    {
        parent::__construct($registry);
        $this->handlersLocator = $handlersLocator;
        $this->logger = new NullLogger();
    }

    /**
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingTraversableParameterTypeHintSpecification
     */
    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        /** @var DoctrineTransport $doctrineTransport */
        $doctrineTransport = parent::createTransport($dsn, $options, $serializer);

        if (strpos($dsn, 'queue_name=outbox') !== false) {
            return new DoctrineTransportOutboxDecorator($doctrineTransport, $this->handlersLocator, $this->logger);
        }

        return $doctrineTransport;
    }
}
