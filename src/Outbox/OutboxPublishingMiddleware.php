<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Outbox;

use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\RecoverableMessageHandlingException;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Throwable;
use WebSupport\Messenger\Message\PublicEvent;
use WebSupport\Messenger\Message\RoutingKeyGenerator;

use function get_class;

/**
 * Runs when worker reads events from the `outbox` transport.
 * Dispatches domain events.
 */
class OutboxPublishingMiddleware implements MiddlewareInterface
{
    use LoggerAwareTrait;

    private TransportInterface $domainEvents;

    public function __construct(TransportInterface $domainEvents)
    {
        $this->logger = new NullLogger();
        $this->domainEvents = $domainEvents;
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        /** @var ReceivedStamp $receivedStamp */
        $receivedStamp = $envelope->last(ReceivedStamp::class);

        if ($receivedStamp !== null && $receivedStamp->getTransportName() === 'outbox') {
            $message = $envelope->getMessage();
            $this->logger->debug('Publishing {visibility} event {class} from outbox', [
                'visibility' => $message instanceof PublicEvent ? 'public' : 'private',
                'class' => get_class($message),
            ]);

            try {
                return $this->domainEvents->send(
                    $envelope->with(new AmqpStamp(RoutingKeyGenerator::generateRoutingKey(get_class($message))))
                );
            } catch (Throwable $throwable) {
                throw new RecoverableMessageHandlingException('Publishing from outbox failed', 0, $throwable);
            }
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
