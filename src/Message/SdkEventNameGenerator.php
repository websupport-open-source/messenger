<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Message;

use WebSupport\Messenger\BoundedContext\BoundedContextNameParser;

final class SdkEventNameGenerator
{
    public static function generateSdkEventName(string $messageClass): string
    {
        $namespace = BoundedContextNameParser::parseNamespace($messageClass);

        $eventClass = array_pop($namespace);

        $eventNamespace = [
            $namespace[0],
            $namespace[1],
            'Sdk',
            'Event',
            $eventClass
        ];

        return implode('\\', $eventNamespace);
    }
}
