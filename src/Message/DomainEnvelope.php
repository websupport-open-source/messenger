<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Message;

use Symfony\Component\Messenger\Envelope;

trait DomainEnvelope
{
    private function createTrackedEnvelope(
        string $aggregateId,
        object $message,
        string $messageType
    ): Envelope {
        $stamp = new DomainMessageStamp($aggregateId, $messageType);

        if ($message instanceof Envelope) {
            return $message->with($stamp);
        }

        return Envelope::wrap($message, [$stamp]);
    }
}
