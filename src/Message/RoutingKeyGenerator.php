<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Message;

use WebSupport\Messenger\BoundedContext\BoundedContextNameParser;

use function end;
use function sprintf;
use function Symfony\Component\String\u;

final class RoutingKeyGenerator
{
    public static function generateRoutingKey(string $eventClass): string
    {
        $contextName = BoundedContextNameParser::parseFromClassName($eventClass);

        $classParts = explode('\\', $eventClass);
        $eventName = u(end($classParts))->camel();

        return sprintf('%s.%s', $contextName, $eventName);
    }
}
