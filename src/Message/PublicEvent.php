<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Message;

/**
 * Public event, can be handled anywhere, including distinct Bounded Contexts
 */
interface PublicEvent extends Event
{
}
