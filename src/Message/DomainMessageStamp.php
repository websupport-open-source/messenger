<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Message;

use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Stamp\StampInterface;

class DomainMessageStamp implements StampInterface
{
    public const MESSAGE_TYPE_EVENT = 'event';
    public const MESSAGE_TYPE_COMMAND = 'command';

    private string $aggregateId;
    private string $messageId;
    private string $messageType;

    public function __construct(string $aggregateId, string $messageType)
    {
        $this->aggregateId = $aggregateId;
        $this->messageId = Uuid::uuid4()->toString();
        $this->messageType = $messageType;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getMessageId(): string
    {
        return $this->messageId;
    }

    public function getMessageType(): string
    {
        return $this->messageType;
    }
}
