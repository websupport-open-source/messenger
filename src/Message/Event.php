<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Message;

/**
 * Private event, can be handled only within the Bounded Context
 */
interface Event
{
}
