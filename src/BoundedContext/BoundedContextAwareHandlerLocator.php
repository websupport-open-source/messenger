<?php

namespace WebSupport\Messenger\BoundedContext;

use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpReceivedStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Handler\HandlersLocator;

class BoundedContextAwareHandlerLocator extends HandlersLocator
{
    public function getHandlers(Envelope $envelope): iterable
    {
        $amqpStamp = $envelope->last(AmqpReceivedStamp::class);

        // DoctrineTransportOutboxDecorator looks for handlers through this locator
        // when saving events to outbox from within another event handler on the event.bus,
        // thus the new message can not have have any Amqp stamp yet
        if ($amqpStamp === null) {
            return yield from parent::getHandlers($envelope);
        }

        assert($amqpStamp instanceof AmqpReceivedStamp);
        $queueBoundedContext = BoundedContextNameParser::parseFromQueueName($amqpStamp->getQueueName());

        foreach (parent::getHandlers($envelope) as $handlerDescriptor) {
            $handlerBoundedContext = BoundedContextNameParser::parseFromMessengerHandlerDescriptor($handlerDescriptor);

            if ($handlerBoundedContext === $queueBoundedContext) {
                yield $handlerDescriptor;
            }
        }
    }
}
