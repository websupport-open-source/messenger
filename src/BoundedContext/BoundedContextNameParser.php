<?php

declare(strict_types=1);

namespace WebSupport\Messenger\BoundedContext;

use InvalidArgumentException;
use Symfony\Component\Messenger\Handler\HandlerDescriptor;

class BoundedContextNameParser
{
    /**
     * Event/Handler class name must have at least three parts - Vendor\BoundedContext\(Event|Handler)Name
     * e.g. WebSupport\Hosting\HostingCreated or WebSupport\Hosting\Domain\Hosting\HostingCreated
     * or handlers WebSupport\Hosting\SomeHandler or WebSupport\Hosting\Application\EventHandler\LinuxHostingHandler
     */
    private const CLASS_PART_COUNT = 3;

    public static function parseNamespace(string $className): array
    {
        $classParts = explode('\\', $className);

        // checks that explode() returned three parts, and not less
        if (count($classParts) < self::CLASS_PART_COUNT) {
            throw new InvalidArgumentException(
                sprintf('Class %s must follow the Vendor\BoundedContext pattern', $className)
            );
        }

        return $classParts;
    }

    public static function parseFromClassName(string $className): string
    {
        $namespace = self::parseNamespace($className);

        // WebSupport\{BoundedContext}\...
        return strtolower($namespace[1]);
    }

    public static function parseFromMessengerHandlerDescriptor(HandlerDescriptor $handlerDescriptor): string
    {
        if (preg_match('/^(?<className>[\w\\\]+)::/', $handlerDescriptor->getName(), $handlerMatches) === 0) {
            throw new InvalidArgumentException(
                sprintf('Could not parse class name from handler descriptor name %s', $handlerDescriptor->getName())
            );
        }

        return self::parseFromClassName($handlerMatches['className']);
    }

    public static function parseFromQueueName(string $queueName): string
    {
        if (preg_match('/^biz-(?<context>\w+)-events/', $queueName, $queueMatches) === 0) {
            throw new InvalidArgumentException(
                sprintf('Event queue name must start with "biz-{context}-events", %s given', $queueName)
            );
        }

        return $queueMatches['context'];
    }
}
