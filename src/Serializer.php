<?php

declare(strict_types=1);

namespace WebSupport\Messenger;

use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\Serializer as BaseSerializer;
use WebSupport\Messenger\Message\PublicEvent;
use WebSupport\Messenger\Message\SdkEventNameGenerator;

class Serializer extends BaseSerializer
{
    /** @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingTraversableReturnTypeHintSpecification */
    public function encode(Envelope $envelope): array
    {
        $encoded = parent::encode($envelope);

        if ($envelope->getMessage() instanceof PublicEvent) {
            $messageClass = get_class($envelope->getMessage());
            $messageType = SdkEventNameGenerator::generateSdkEventName($messageClass);

            $encoded['headers']['type'] = $messageType;
        }

        return $encoded;
    }
}
