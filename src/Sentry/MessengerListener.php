<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Sentry;

use Sentry\State\HubInterface;
use Sentry\State\Scope;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpReceivedStamp;
use Symfony\Component\Messenger\Event\WorkerMessageFailedEvent;
use Symfony\Component\Messenger\Event\WorkerMessageHandledEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Stamp\BusNameStamp;

/**
 * Adds more information about failed messages
 * Replaces @see \Sentry\SentryBundle\EventListener\MessengerListener
 * Related MR https://github.com/getsentry/sentry-symfony/pull/492
 */
final class MessengerListener
{
    private HubInterface $hub;

    /**
     * @var bool Whether to capture errors thrown while processing a message that
     *           will be retried
     */
    private bool $captureSoftFails;

    public function __construct(HubInterface $hub, bool $captureSoftFails = true)
    {
        $this->hub = $hub;
        $this->captureSoftFails = $captureSoftFails;
    }

    /**
     * This method is called for each message that failed to be handled.
     *
     * @param WorkerMessageFailedEvent $event The event
     */
    public function handleWorkerMessageFailedEvent(WorkerMessageFailedEvent $event): void
    {
        if (!$this->captureSoftFails && $event->willRetry()) {
            return;
        }

        $error = $event->getThrowable();

        if ($error instanceof HandlerFailedException) {
            $envelope = $error->getEnvelope();
            $transportName = $event->getReceiverName();
            $this->hub->withScope(function (Scope $scope) use ($error, $envelope, $transportName) {
                $scope->setExtra('messenger.transport', $transportName);

                $busStamp = $envelope->last(BusNameStamp::class);
                if ($busStamp !== null) {
                    assert($busStamp instanceof BusNameStamp);
                    $scope->setExtra('messenger.message_bus', $busStamp->getBusName());
                }

                $message = $envelope->getMessage();
                $scope->setExtra('messenger.message_class', get_class($message));

                $amqpReceivedStamp = $envelope->last(AmqpReceivedStamp::class);
                if ($amqpReceivedStamp !== null) {
                    assert($amqpReceivedStamp instanceof AmqpReceivedStamp);
                    $amqpEnvelope = $amqpReceivedStamp->getAmqpEnvelope();
                    $scope->setExtra('amqp.queue', $amqpReceivedStamp->getQueueName());
                    $scope->setExtra('amqp.routing_key', $amqpEnvelope->getRoutingKey());
                    $scope->setExtra('amqp.body', $amqpEnvelope->getBody());
                }

                foreach ($error->getNestedExceptions() as $nestedException) {
                    $this->hub->captureException($nestedException);
                }
            });
        } else {
            $this->hub->captureException($error);
        }

        $this->flushClient();
    }

    /**
     * This method is called for each handled message.
     *
     * @param WorkerMessageHandledEvent $event The event
     */
    public function handleWorkerMessageHandledEvent(WorkerMessageHandledEvent $event): void
    {
        // Flush normally happens at shutdown... which only happens in the worker if it is run with a lifecycle limit
        // such as --time=X or --limit=Y. Flush immediately in a background worker.
        $this->flushClient();
    }

    private function flushClient(): void
    {
        $client = $this->hub->getClient();

        if (null !== $client) {
            $client->flush();
        }
    }
}
