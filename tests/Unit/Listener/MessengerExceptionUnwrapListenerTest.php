<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Listener;

use LogicException;
use PHPUnit\Framework\TestCase;
use SebastianBergmann\Type\RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use WebSupport\Messenger\Listener\MessengerExceptionUnwrapListener;

class MessengerExceptionUnwrapListenerTest extends TestCase
{
    private MessengerExceptionUnwrapListener $listener;

    public function setUp(): void
    {
        $this->listener = new MessengerExceptionUnwrapListener();
    }

    /**
     * @dataProvider getExceptionEvents
     */
    public function testUnwrapHandlerFailedException(ExceptionEvent $exceptionEvent): void
    {
        $this->listener->unwrapException($exceptionEvent);
        $this->assertInstanceOf(RuntimeException::class, $exceptionEvent->getThrowable());
    }

    public function getExceptionEvents(): array
    {
        return [
            [new ExceptionEvent(
                $this->createMock(HttpKernelInterface::class),
                new Request(),
                HttpKernelInterface::MAIN_REQUEST,
                new HandlerFailedException($this->getDummyEnvelope(), [new RuntimeException()])
            )],
            [new ExceptionEvent(
                $this->createMock(HttpKernelInterface::class),
                new Request(),
                HttpKernelInterface::MAIN_REQUEST,
                new HandlerFailedException($this->getDummyEnvelope(),
                    [new UnrecoverableMessageHandlingException('foo', 0, new RuntimeException())])
            )],
            [new ExceptionEvent(
                $this->createMock(HttpKernelInterface::class),
                new Request(),
                HttpKernelInterface::MAIN_REQUEST,
                new RuntimeException('foo', 0, new LogicException())
            )],
        ];
    }

    private function getDummyEnvelope(): Envelope
    {
        return new Envelope(new class {});
    }
}
