<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Listener;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Generator;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Event\WorkerMessageFailedEvent;
use Symfony\Component\Messenger\Event\WorkerRunningEvent;
use Symfony\Component\Messenger\Worker;
use WebSupport\Messenger\Listener\StopWorkerOnClosedEntityManagerListener;

final class StopWorkerOnClosedEntityManagerListenerTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @dataProvider entityManagerClosedProvider
     */
    public function testWorkerStopsWhenMaximumCountReached(bool $entityManagerClosed): void
    {
        $worker = Mockery::mock(Worker::class);
        $logger = Mockery::mock(LoggerInterface::class);
        $em = Mockery::mock(EntityManagerInterface::class);
        $em->shouldReceive(['isOpen' => !$entityManagerClosed])->once();
        if ($entityManagerClosed) {
            $worker->shouldReceive('stop')->once();
            $logger->shouldReceive('warning')->once();
        }

        $failedEvent = $this->createFailedEvent();
        $runningEvent = new WorkerRunningEvent($worker, false);

        $failureLimitListener = new StopWorkerOnClosedEntityManagerListener($em, $logger);
        $failureLimitListener->onMessageFailed($failedEvent);
        $failureLimitListener->onWorkerRunning($runningEvent);
    }

    public function entityManagerClosedProvider(): Generator
    {
        yield [false];
        yield [true];
    }

    private function createFailedEvent(): WorkerMessageFailedEvent
    {
        $envelope = new Envelope(new class ( ) { });

        return new WorkerMessageFailedEvent($envelope, 'default', ORMException::entityManagerClosed());
    }
}
