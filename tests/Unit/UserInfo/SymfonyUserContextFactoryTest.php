<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\UserInfo;

use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\InMemoryUser;
use WebSupport\Messenger\UserInfo\SymfonyUserContextFactory;

class SymfonyUserContextFactoryTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function testCreateContext(): void
    {
        $login = 'dummy.login';
        $ip = '37.9.169.143';

        $factory = new SymfonyUserContextFactory(
            $this->mockRequestStack($ip),
            $this->mockSecurity($login)
        );

        $context = $factory->createUserContext();
        $this->assertEquals($login, $context->getLogin());
        $this->assertEquals($ip, $context->getIp());
        $this->assertEquals('', $context->getId());
    }

    private function mockSecurity(string $expectedLogin): Security
    {
        $security = Mockery::mock(Security::class);
        $security->shouldReceive('getUser')
            ->andReturn(new InMemoryUser($expectedLogin, null))->once();

        return $security;
    }

    private function mockRequestStack(string $expectedIp): RequestStack
    {
        $requestStack = Mockery::mock(RequestStack::class);

        $request = Request::create('/foo', 'GET', [], [], [], [
            'REMOTE_ADDR' => '1.1.1.1',
            'HTTP_X_FORWARDED_FOR' => $expectedIp,
        ]);
        $request->overrideGlobals();
        Request::setTrustedProxies(['REMOTE_ADDR'], Request::HEADER_X_FORWARDED_FOR);

        $requestStack->shouldReceive('getMasterRequest')
            ->andReturn($request)
            ->once();

        return $requestStack;
    }
}
