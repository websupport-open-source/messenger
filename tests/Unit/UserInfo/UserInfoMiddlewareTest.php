<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\UserInfo;

use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use stdClass;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Middleware\StackMiddleware;
use Symfony\Component\Messenger\Stamp\StampInterface;
use Symfony\Component\Messenger\Test\Middleware\MiddlewareTestCase;
use WebSupport\Messenger\UserInfo\UserContext;
use WebSupport\Messenger\UserInfo\UserContextFactory;
use WebSupport\Messenger\UserInfo\UserInfoMiddleware;
use WebSupport\Messenger\UserInfo\UserInfoStamp;

class UserInfoMiddlewareTest extends MiddlewareTestCase
{
    use MockeryPHPUnitIntegration;

    private string $login = 'dummy.login';
    private string $ip = '37.9.169.143';
    private string $id = '100200';

    public function testFreshCommandGetsUserInfoAndProvidesContextToSuccessiveMessages(): void
    {
        $contextFactory = Mockery::mock(UserContextFactory::class);
        $contextFactory
            ->shouldReceive('createUserContext')
            ->andReturn(new UserContext($this->id, $this->ip, $this->login))
            ->once();

        $middleware = new UserInfoMiddleware($contextFactory);

        // we dispatch command
        $envelope = $middleware->handle(
            new Envelope(new stdClass()),
            $this->createStackWithNextMiddleware($middleware)
        );

        $this->assertUserInfo($envelope->last(UserInfoStamp::class));
    }

    public function testMessageWithStampProvidesContextToSuccessiveMessages(): void
    {
        $middleware = new UserInfoMiddleware(Mockery::mock(UserContextFactory::class));

        // we get external message (foreign event) with some context
        $envelope = $middleware->handle(
            new Envelope(new stdClass(), [new UserInfoStamp($this->id, $this->ip, $this->login)]),
            $this->createStackWithNextMiddleware($middleware)
        );

        $this->assertUserInfo($envelope->last(UserInfoStamp::class));
    }



    private function assertUserInfo(?StampInterface $userInfo): void
    {
        $this->assertNotNull($userInfo);
        assert($userInfo instanceof UserInfoStamp);

        $this->assertEquals($this->id, $userInfo->getId());
        $this->assertEquals($this->login, $userInfo->getLogin());
        $this->assertEquals($this->ip, $userInfo->getIp());
    }

    private function createStackWithNextMiddleware(UserInfoMiddleware $middleware): StackMiddleware
    {
        $nextMiddleware = Mockery::mock(MiddlewareInterface::class);
        $nextMiddleware
            ->shouldReceive('handle')
            ->once()
            ->andReturnUsing(function (Envelope $envelope, StackInterface $stack) use ($middleware): Envelope {

                // this runs within the handling of the original message
                // it's like sending an event message while command is handled
                $eventEnvelope = $middleware->handle(new Envelope(new stdClass()), $this->getStackMock());

                $this->assertUserInfo($eventEnvelope->last(UserInfoStamp::class));

                return $envelope;
            });

        return new StackMiddleware($nextMiddleware);
    }
}
