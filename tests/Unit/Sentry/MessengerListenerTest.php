<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Sentry;

use GuzzleHttp\Promise\FulfilledPromise;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Sentry\Client;
use Sentry\Event;
use Sentry\Options;
use Sentry\State\Hub;
use Sentry\Transport\TransportInterface;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpReceivedStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Event\WorkerMessageFailedEvent;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Stamp\BusNameStamp;
use WebSupport\Messenger\Sentry\MessengerListener;

class MessengerListenerTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function testHandleWorkerMessageFailedEventWithStamps(): void
    {
        $busName = 'fooBus';
        $amqpEnvelope = Mockery::mock(\AMQPEnvelope::class);
        $queueName = 'foo-events';
        $routingKey = 'foo.bar';
        $amqpEnvelope->shouldReceive('getRoutingKey')->andReturn($routingKey)->once();
        $body = '{"foo": "bar"}';
        $amqpEnvelope->shouldReceive('getBody')->andReturn($body)->once();
        $envelope = Envelope::wrap(new \stdClass(), [
            new BusNameStamp('fooBus'),
            new AmqpReceivedStamp($amqpEnvelope, $queueName)
        ]);

        $transport = 'async';
        $event = new WorkerMessageFailedEvent($envelope, $transport, new HandlerFailedException($envelope, [new \Exception('Dummy error')]));

        $transportMock = Mockery::mock(TransportInterface::class);
        $transportMock->shouldReceive('close');
        $transportMock->shouldReceive('send')->with(Mockery::on(function (Event $event) use ($body, $queueName, $routingKey, $busName, $transport) {
            $extra = $event->getExtra();
            return $extra['messenger.transport'] === $transport
                && $extra['messenger.message_class'] === 'stdClass'
                && $extra['messenger.message_bus'] === $busName
                && $extra['amqp.routing_key'] === $routingKey
                && $extra['amqp.queue'] === $queueName
                && $extra['amqp.body'] === $body

            ;
        }))->andReturn(new FulfilledPromise(true))->once();

        $client = new Client(new Options([]), $transportMock);
        $hub = new Hub($client);
        $listener = new MessengerListener($hub);

        $listener->handleWorkerMessageFailedEvent($event);
    }
}
