<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Correlation;

use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use Monolog\Test\TestCase as MonologTestCase;
use WebSupport\Messenger\Correlation\CorrelationIdProvider;
use WebSupport\Messenger\Correlation\MessageLogProcessor;
use WebSupport\Messenger\Message\DomainMessageStamp;
use WebSupport\Messenger\Message\Event;

class MessageLogProcessorTest extends MonologTestCase
{
    use MockeryPHPUnitIntegration;

    public function testMonologStack(): void
    {
        $handler = new TestHandler();
        $idProvider = new CorrelationIdProvider();

        $messageLogProcessor = new MessageLogProcessor($idProvider);
        $handler->pushProcessor($messageLogProcessor);

        $idProvider->addMessageId('some-command-id');
        $eventStamp = new DomainMessageStamp('aggregate-id', DomainMessageStamp::MESSAGE_TYPE_EVENT);
        $idProvider->addMessageFromStamp($eventStamp);

        $handler->handle(array_merge($this->getRecord(Logger::WARNING, 'test', [
            'message' => new class implements Event {},
        ]), [
            'channel' => MessageLogProcessor::MONOLOG_CHANNEL,
        ]));

        [$record] = $handler->getRecords();
        $this->assertEquals('aggregate-id', $record['extra']['aggregate_id']);
        $this->assertEquals('event', $record['extra']['msg_type']);
        $this->assertEquals($eventStamp->getMessageId(), $record['extra']['msg_id']);
        $this->assertEquals('some-command-id', $record['extra']['msg_cause_id']);
    }
}
