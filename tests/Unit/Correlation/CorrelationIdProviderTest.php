<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Correlation;

use PHPUnit\Framework\TestCase;
use WebSupport\Messenger\Correlation\CorrelationIdProvider;
use WebSupport\Messenger\Message\DomainMessageStamp;

class CorrelationIdProviderTest extends TestCase
{
    /**
     * Test that causation message matches for the following causes and is null otherwise
     *  command-1 causes event-1 and event-3
     *  event-1 causes event-2
     */
    public function testCausationVector(): void
    {
        $idProvider = new CorrelationIdProvider();
        $this->assertNull($idProvider->getCausationMessageId());

        $idProvider->addMessageId('command-1');
        $this->assertNull($idProvider->getCausationMessageId());

        $idProvider->addMessageId('event-1');
        $this->assertEquals('command-1', $idProvider->getCausationMessageId());

        $idProvider->addMessageId('event-2');
        $this->assertEquals('event-1', $idProvider->getCausationMessageId());

        $idProvider->end();
        $this->assertEquals('command-1', $idProvider->getCausationMessageId());

        $idProvider->end();
        $this->assertNull($idProvider->getCausationMessageId());

        $idProvider->addMessageId('event-3');
        $this->assertEquals('command-1', $idProvider->getCausationMessageId());

        $idProvider->end();
        $this->assertNull($idProvider->getCausationMessageId());

        $idProvider->end();
        $this->assertNull($idProvider->getCausationMessageId());
    }

    public function testCausationFromStamp(): void
    {
        $idProvider = new CorrelationIdProvider();
        $this->assertNull($idProvider->getCurrentAggregateId());
        $this->assertNull($idProvider->getCurrentMessageId());

        $stamp = new DomainMessageStamp('aggregate-id', DomainMessageStamp::MESSAGE_TYPE_EVENT);
        $idProvider->addMessageFromStamp($stamp);

        $this->assertEquals('aggregate-id', $idProvider->getCurrentAggregateId());
        $this->assertEquals($stamp->getMessageId(), $idProvider->getCurrentMessageId());
        $this->assertEquals($stamp->getMessageType(), $idProvider->getCurrentMessageType());
        $this->assertNull($idProvider->getCausationMessageId());
    }
}
