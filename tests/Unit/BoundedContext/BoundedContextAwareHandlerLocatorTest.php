<?php

namespace WebSupport\Messenger\Tests\Unit\BoundedContext;

use AMQPEnvelope;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpReceivedStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Handler\HandlerDescriptor;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;
use WebSupport\Hosting\Tests\Dummy\DummyHostingEvent;
use WebSupport\Hosting\Tests\Dummy\DummyHostingHandler;
use WebSupport\Messenger\BoundedContext\BoundedContextAwareHandlerLocator;
use WebSupport\Messenger\Message\DomainMessageStamp;
use WebSupport\Services\Tests\Dummy\DummyServicesHandler;

class BoundedContextAwareHandlerLocatorTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function testOnlyHandlersFromContextMatchingQueueAreReturned(): void
    {
        $envelope = new Envelope(new DummyHostingEvent(), [
            new DomainMessageStamp('foo', DomainMessageStamp::MESSAGE_TYPE_EVENT),
            new ReceivedStamp('domainEvents'),
            new AmqpReceivedStamp(new AMQPEnvelope(), 'biz-hosting-events')
        ]);

        $locator = new BoundedContextAwareHandlerLocator([
            DummyHostingEvent::class => [
                new HandlerDescriptor(new DummyServicesHandler()),
                new HandlerDescriptor(new DummyHostingHandler()),
            ]
        ]);

        $relevantHandlers = iterator_to_array($locator->getHandlers($envelope));
        $this->assertCount(1, $relevantHandlers);
        $this->assertInstanceOf(DummyHostingHandler::class, $relevantHandlers[0]->getHandler());
    }
}
