<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\BoundedContext;

use PHPUnit\Framework\TestCase;
use WebSupport\Hosting\Tests\Unit\BoundedContext\DummyHostingEvent;
use WebSupport\Messenger\BoundedContext\BoundedContextNameParser;

class BoundedContextNameParserTest extends TestCase
{
    public function testParseFromEvent(): void
    {
        $this->assertEquals('hosting', BoundedContextNameParser::parseFromClassName(DummyHostingEvent::class));
    }
}
