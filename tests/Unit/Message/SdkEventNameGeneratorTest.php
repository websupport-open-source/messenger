<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Message;

use PHPUnit\Framework\TestCase;
use WebSupport\Messenger\Message\SdkEventNameGenerator;

class SdkEventNameGeneratorTest extends TestCase
{
    /**
     * @dataProvider getEvents
     */
    public function testGenerateSdkEventName($domainEventName, $sdkEventName): void
    {
        $sdkClass = SdkEventNameGenerator::generateSdkEventName($domainEventName);

        $this->assertEquals($sdkEventName, $sdkClass);
    }

    public function testGenerateSdkEventNameShouldFailForInvalidNamespaces(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        SdkEventNameGenerator::generateSdkEventName('Invalid\Namespace');
    }

    public function getEvents()
    {
        return [
            ['WebSupport\Payment\Sdk\Event\Domain\Payment\PaymentReceived', 'WebSupport\Payment\Sdk\Event\PaymentReceived'],
            ['WebSupport\Service\MyEvent', 'WebSupport\Service\Sdk\Event\MyEvent'],

        ];
    }
}
