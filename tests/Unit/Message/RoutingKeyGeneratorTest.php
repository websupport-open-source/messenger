<?php

namespace WebSupport\Messenger\Tests\Unit\Message;

use PHPUnit\Framework\TestCase;
use WebSupport\Hosting\Tests\Dummy\DomainAssignedToLinuxHosting;
use WebSupport\Hosting\Tests\Dummy\HostingCreated;
use WebSupport\Messenger\Message\RoutingKeyGenerator;

class RoutingKeyGeneratorTest extends TestCase
{
    public function testRoutingKeyGenerator(): void
    {
        $routingKey = RoutingKeyGenerator::generateRoutingKey(HostingCreated::class);
        $this->assertEquals('hosting.hostingCreated', $routingKey);

        $routingKey = RoutingKeyGenerator::generateRoutingKey(DomainAssignedToLinuxHosting::class);
        $this->assertEquals('hosting.domainAssignedToLinuxHosting', $routingKey);
    }
}
