<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Changelog;

class DummyMessage
{
    private int $id;

    private string $foo;

    public function __construct(int $id, string $foo)
    {
        $this->foo = $foo;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFoo(): string
    {
        return $this->foo;
    }
}
