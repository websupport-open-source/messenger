<?php

declare(strict_types=1);

namespace WebSupport\Messenger\Tests\Unit\Changelog;

use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Test\Middleware\MiddlewareTestCase;
use Symfony\Component\Serializer\SerializerInterface;
use WebSupport\Messenger\Changelog\ChangelogMiddleware;
use WebSupport\Messenger\Changelog\ChangelogStamp;
use WebSupport\Messenger\Message\DomainMessageStamp;

class ChangelogMiddlewareTest extends MiddlewareTestCase
{
    use MockeryPHPUnitIntegration;

    public function testHandleEventWithLogging(): void
    {
        $loggerMock = Mockery::mock(LoggerInterface::class);
        $loggerMock->shouldReceive('info')->with('{"id":1,"foo":"bar"}',
            ['id' => '1', 'context' => 'messenger', 'messageType' => 'event', 'messageName' => 'DummyMessage'])->once();
        $serializerMock = Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldReceive('serialize')->andReturn('{"id":1,"foo":"bar"}')->once();
        $middleware = new ChangelogMiddleware($loggerMock, $serializerMock);

        $dummyEnvelope = new Envelope(new DummyMessage(1, 'bar'),
            [new DomainMessageStamp('1', DomainMessageStamp::MESSAGE_TYPE_EVENT)]);
        $stack = $this->getStackMock();
        $middleware->handle($dummyEnvelope, $stack);
    }

    /**
     * @dataProvider unsuccessfulEnvelope
     */
    public function testHandleEventWithoutLogging(Envelope $envelope): void
    {
        $loggerMock = Mockery::mock(LoggerInterface::class);
        $loggerMock->shouldNotReceive('info');
        $serializerMock = Mockery::mock(SerializerInterface::class);
        $serializerMock->shouldNotReceive('serialize');
        $middleware = new ChangelogMiddleware($loggerMock, $serializerMock);

        $stack = $this->getStackMock();
        $middleware->handle($envelope, $stack);
    }

    public function unsuccessfulEnvelope(): array
    {
        return [
            [
                new Envelope(new DummyMessage(1, 'bar'), [
                    new DomainMessageStamp('1', DomainMessageStamp::MESSAGE_TYPE_EVENT),
                    new ChangelogStamp(),
                ]),
            ],
            [
                new Envelope(new DummyMessage(1, 'bar')),
            ],

        ];
    }
}
