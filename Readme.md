# Messenger

[[_TOC_]]

To install, run
```
composer require websupport/messenger
```

## Bounded Contexts
Received events are read from the AMQP queue names `biz-{context}-events`. The `{context}` corresponds to the receiving context. This context is used to restrict message handlers so that no other handlers that may match the class-name (or ancestors) are executed. 

Example: Origin application dispatches `WebSupport\Payment\PaymentReceived` event message. `RoutingKeyGenerator` configures routing key as `payment.paymeny_received`. This message is routed through the topic exchange (`domain-events`) to the subscribed queue `biz-hosting-events`. `BoundedContextAwareHandlerLocator` looks at the queue name and determines that only handlers defined in `WebSupport\Hosting` namespace can handle this message.

## Event Publishing
To keep strict transactional consistency with the at-least-once message semantics all events go to `outbox` transport, which is configured to persist events in the database together with the domain entities. `OutboxPublishingMiddleware` forwards these events to the `domainEvents` transport that is configured as RabbitMq topic exchange, which then routes message to all declared queues.

To route events to a desired transport a `DoctrineTransportOutboxDecorator` decorates `DoctrineTransport` for the `outbox`. In Messenger, events would not reach `outbox` if they don't have at least one handler declared. The decorator makes sure to save an event to the database even if it does not have any local handler defined.

The decorator also makes sure, that events invoke only handlers within the Bounded Context of the event itself. That means they are private to Bounded Context by default. You can make an event public to be consumed by foreign BC.

## Message logs
All domain messages (commands & events) must have a `DomainMessageStamp`. It attaches some meta-data, like a unique message id. `CorrelationLogMiddleware` uses `CorrelationIdProvider` to keep track of handled messages and monitors the correlation between them as they flow through buses. A `MessageLogProcessor` then attaches all collected meta-data to all logs as extra data.

The result is that, for example the log message `Message {class} handled by {handler}` now contains the unique id of the message, type of the message (whether it's event or command), the id of the message that cause it, and the id of the aggregate it touches (either target aggregate id for commands, or the id of the aggregate which produced the message in case of events).

## Changelog

`ChangelogMiddleware` logs domain messages - command and events (that have `DomainMessageStamp`) into ElasticSearch index `docker-php-json-changelog`. This ES changelog is used to cumulate info from multiple services (e.g. services, payment, frm...). Helpdesk queries this changelog to see for example what happened to a user's service. The structure of log entry is as follows:

```
'id' => aggregate id
'context' => name of bounded context (e.g. Hosting)
'messageType' => type of message (command/event)
'messageName' => name of message (e.g. HostingDeleted)
'userLogin' => login of the user trigerring the message
'userId' => user id of the user trigerring the message
'userIp' => ip address of the user trigerring the message 
```

## User information

`UserInfoMiddleware` gathers `UserContext` and appends `UserInfoStamp` to all messages when they are seen for the first time (usually when the message is dispatched). Package contains Symfony-specific `UserContextFactory` implementation that uses JWT-provided user login fetched from the `Security` component and parses `X-Forwarded-For` from the HTTP current request to provide user's ip address. Symfony implementation currently does not provide user id because requests from generated SDKs don't contain information about user id.  

## Events

Read mode about [event-driven architecture at WebSupport on confluence](https://loopiagroup.atlassian.net/wiki/spaces/ITD/pages/9371861/Commands+Events).

All event message classes must implement either `Event` or `PublicEvent` marker interface.

Event classes are normally part of the domain layer and properties have normally non-primitive types like Value Objects and Enums, e.g.:

```php
namespace WebSupport\Hosting\Domain;

use WebSupport\Messenger\Message\PublicEvent;

class HostingRestored implements PublicEvent
{
  private HostingId $hostingId;
  private RunningState $state;
  .... 
}
```

Public events cannot be directly used in different applications because they may use other project-specific classes. Public events define schema for the message indirectly. For all public events (events designed to be consumed by other application), an SDK counterpart must be created. `SdkEventNameGenerator` generates FQCN automatically by adding SDK to namespace. For example the above event should have a following class in `sdk/Event/HostingRestored.php`:

```php
namespace WebSupport\Hosting\Sdk\Event;

class HostingRestored
{
  private int $hostingId;
  private string $state;
  .... 
}
```

### Future work
SDK event classes can be generated the same way we generate HTTP client. Events tagged with `PublicEvent` can export OA JSON Schema API description. Jane generator can use it to generate event classes.

## Sentry error listener
`MessengerListener` provides additional information for sentry events (message class, body, routing key, etc...)
